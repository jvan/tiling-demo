var coffee, gulp, gutil, livereload, watch;

gulp = require('gulp');

coffee = require('gulp-coffee');

livereload = require('gulp-livereload');

watch = require('gulp-watch');

gutil = require('gulp-util');

livereload.listen();

gulp.task('reload', function() {
  return gulp.src('./*.html').pipe(livereload());
});

gulp.task('coffee', function() {
  return gulp.src('./*.coffee').pipe(coffee({
    bare: true
  }).on('error', gutil.log)).pipe(gulp.dest('./')).pipe(livereload());
});

gulp.task('watch', function() {
  gulp.watch('./*.html', ['reload']);
  return gulp.watch('./*.coffee', ['coffee']);
});

gulp.task('default', ['coffee', 'watch']);
