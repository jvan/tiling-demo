gulp = require 'gulp'

coffee     = require 'gulp-coffee'
livereload = require 'gulp-livereload'
watch      = require 'gulp-watch'
gutil      = require 'gulp-util'

livereload.listen()

gulp.task 'reload', ->
   gulp.src('./*.html')
      .pipe(livereload())

gulp.task 'coffee', ->
   gulp.src('./*.coffee')
      .pipe(coffee(bare: true).on('error', gutil.log))
      .pipe(gulp.dest('./'))
      .pipe(livereload())

gulp.task 'watch', ->
   gulp.watch './*.html',   ['reload']
   gulp.watch './*.coffee', ['coffee']

gulp.task 'default', ['coffee', 'watch']
