#------------------------------------------------------------
# Utility Functions
#
#------------------------------------------------------------

random = (min, max) ->
   min + (max-min) * Math.random()

distance = (p0, p1) ->
   dx = p0[0] - p1[0]
   dy = p0[1] - p1[1]

   Math.sqrt(dx*dx + dy*dy)

draw_tile = (svg, i, j, size, color) ->
   x = i * size
   y = j * size

   svg.append('rect')
      .attr('class', 'tile')
      .attr("x", x)
      .attr("y", y)
      .attr("width",  size)
      .attr("height", size)
      .style("fill", color)


#------------------------------------------------------------
# Ember Application
#
#------------------------------------------------------------

App = Ember.Application.create()


# Template helper for formatting numbers.
formatNumber = (params) ->
   if params[0]
      params[0].toFixed(2)

Ember.HTMLBars._registerHelper 'format-num', Ember.HTMLBars.makeBoundHelper(formatNumber)

# Template helper for computing and formatting percentages.
formatPercent = (params) ->
   if params[0]
      pct = 100.0 * (+params[0] / +params[1] - 1.0)
      html = "<span class='label label-default'>#{pct.toFixed(2)}%</span>"
      new Ember.Handlebars.SafeString html

Ember.HTMLBars._registerHelper 'format-pct', Ember.HTMLBars.makeBoundHelper(formatPercent)


# Tile statistics.
App.Tile = Ember.Object.extend

   #----- Computed Properties ------------------------------

   label: Em.computed 'tileSize', ->
      "#{@get 'tileSize'}x#{@get 'tileSize'}"

   area: Em.computed 'tileSize', 'tileCount', ->
      # Return the total area of all tiles.
      Math.pow(@get('tileSize'), 2) * @get('tileCount')


App.ApplicationRoute = Ember.Route.extend
   model: ->
      Ember.Object.create
         x: 250
         y: 250
         r: 200

App.ApplicationController = Ember.Controller.extend
   width:  500
   height: 500

   actions:
      randomize: ->
         circle = @get 'model'

         circle.setProperties
            x: @get('width') * random(0.4, 0.6)
            y: @get('width') * random(0.4, 0.6)
            r: @get('width') * (0.1 + Math.random() * 0.3)


App.GridTilesComponent = Ember.Component.extend

   #------Properties ---------------------------------------
   attributeBindings: 'width height circle'.w()

   numTiles: 40   # number of (1x1) tiles in the grid
   tiles:  Em.A() # statistics for various grid resolutions
   occupied: { }  # hash of already occupied (1x1) tiles

   #----- Computed Properties ------------------------------

   totalTiles: Em.computed 'tiles.@each.tileCount', ->
      # Return the total number of tiles used to fill the circle.      
      @get('tiles').reduce(((total, tile) -> total + tile.get('tileCount')), 0)

   tileArea: Em.computed 'tiles.@each.area', ->
      # Return the total area of the tiles used to fill the circle.
      @get('tiles').reduce(((total, tile) -> total + +tile.get('area')), 0)

   tilesSaved: Em.computed 'tileArea', 'totalTiles', ->
      @get('tileArea') - @get('totalTiles')

   circleArea: Em.computed 'circle.r', ->
      area = Math.PI * Math.pow(@get('circle.r'), 2)
      tilesPerPixel = @get('numTiles') / @get('width')
      area * Math.pow(tilesPerPixel, 2)

   #----- Observers ----------------------------------------

   updateCircle: Em.observer 'circle.r', ->
      @setProperties
         tiles: Em.A()
         occupied: { }

      d3.selectAll('rect').remove()

      @drawCircle()
      @drawTiles()

   #------ Methods -----------------------------------------
      
   didInsertElement: ->
      # Get the width and height attributes.
      width  = @get 'width'
      height = @get 'height'

      # Initialize the svg element and set the width and height.
      @svg = d3.select('#canvas')
         .attr('width', width)
         .attr('height', height)

      # Create scales and axes.
      @xScale = d3.scale.linear()
         .range([0, width])

      @yScale = d3.scale.linear()
         .range([height, 0])

      @xAxis = d3.svg.axis()
         .scale(@xScale)
         .orient("bottom")
         .ticks(16)

      @yAxis = d3.svg.axis()
         .scale(@yScale)
         .orient("left")
         .ticks(16)

      # Create a color scale.
      @color = d3.scale.category10()
         .domain(d3.range(10))

      @circleElem = @svg.append('circle')

      @draw()


   #------ Render Methods ----------------------------------

   draw: ->
      @drawGrid()
      @drawCircle()
      @drawTiles()


   drawGrid: ->
      # Create a grid using x and y axes. 

      @svg.append('g')
         .attr('class', 'grid')
         .attr('transform', "translate(0, #{@get 'height'})")
         .call(@xAxis.tickSize(-@get('height'), 0, 0)
            .tickFormat(""))

      @svg.append('g')
         .attr('class', 'grid')
         .call(@yAxis.tickSize(-@get('width'), 0, 0)
            .tickFormat(""))

   drawCircle: ->
      @circleElem.attr("cx", @get('circle.x'))
         .attr("cy", @get('circle.y'))
         .attr("r",  @get('circle.r'))

   drawTiles: ->
      # Draw the grid tiles that approximate the circle.

      # Grid resolutions used to fill the circle. The size of the grid is the
      # width measured in 1x1 tiles. So a size 8 grid would measure 8x8 and 
      # contain 64 1x1 tiles.
      #
      # The strict value determines whether strict inclusion is used when 
      # filling the circle.
      grids = [
         { size: 8, strict: true }
         { size: 4, strict: true }
         { size: 2, strict: true }
         { size: 1, strict: false }
      ]

      # Starting with the largest grid size, fill the circle with as many tiles
      # as possible. Then proceed to the next largest grid.
      for grid, i in grids

         count = @fillTiles grid.size, @color(i), grid.strict

         @tiles.pushObject App.Tile.create
            tileSize:  grid.size
            tileCount: count

   fillTiles: (size, color, strictInclusion=true) ->
      
      # Compute the tile size for the current grid resolution.
      numGridTiles = @get('numTiles') / size
      gridSize = @get('width') / numGridTiles

      count = 0 # number of grid tiles that fit inside the circle

      center = [@get('circle.x'), @get('circle.y')] # position of the center of the circle
      radius = @get('circle.r')                     # radius of the circle

      occupied = @get 'occupied'

      # Iterate over all tiles in the grid.
      for i in [0...numGridTiles]
         for j in [0...numGridTiles]

            # Calculate the positions of the corners of the tile.
            x0 = i * gridSize
            y0 = j * gridSize

            points = [
               [x0,          y0]
               [x0+gridSize, y0]
               [x0+gridSize, y0+gridSize]
               [x0,          y0+gridSize]
            ]

            # Check to see if the corners are inside the circle.
            interior = for point in points
               distance(point, center) < radius

            # If strictInclusion is true the tiles is contained in the circle
            # only if all of the corners lie inside the circle. Otherwise, only
            # one corner needs to be interior.
            if strictInclusion
               isContained = not (false in interior)
            else
               isContained = (true in interior)

            # If the tile is contained in the circle we then check to make sure
            # that the cell is not already filled by a tile beloging to a larger
            # grid.
            if isContained

               # Compute the 1x1 grid indices for the current tile.
               tx = i * size
               ty = j * size

               # Generate an array of indices for all 1x1 tiles that fit inside
               # the current tile.
               subTiles = []

               for r in [0...size]
                  for s in [0...size]
                     subTiles.push [tx+s, ty+r]

               # Check to see if any of the sub-tiles are aleady occupied.
               filled = for tile in subTiles
                           key = "#{tile[0]},#{tile[1]}"
                           key of occupied
               
               # If any of the subtiles are already occupied continue to the
               # next tile in the grid.
               if true in filled
                  continue

               # The tile is contained in the circle and is not already part of
               # a larger tile. Draw the tile and increment the count for the 
               # current grid.
               draw_tile(@svg, i, j, gridSize, color)
               count += 1

               # Mark all 1x1 sub-tiles as occupied.
               for tile in subTiles
                  key = "#{tile[0]},#{tile[1]}"
                  occupied[key] = true


      # Return the number of tiles filled for this grid.
      count
