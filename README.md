# tiling demo

## installation

* `npm install`
* `bower install`
* `gulp`

## running

* `python -m http.server`
* view app at [http://localhost:8000](http://localhost:8000)
